<?php

namespace northug\metaTags;

use Yii;
use yii\web\Request;
use yii\db\ActiveRecord;
use northug\metaTags\models\MetaTags;
use northug\metaTags\models\MetaTagsParams;

/**
 * Description of MetaTagBehavior
 *
 * @author Пользователь
 */
class MetaTagBehavior extends \yii\base\Behavior {

    /**
     *  $options = [
     *      'title' => [
     *          'default' => 'titleModel',  // Срабатывает только при не введенном значении title
     *          'value' => function () {    // Срабатывает только при не введенном значении title
     *              return '{attribute1}, какой то текст, {attribute2}'.
     *          }
     *      ],
     *  ];
     * @var array
     */
    public $options;


    public function events() {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterSave($event) {
        if (!Yii::$app->request instanceof Request) {
            return;
        }
        $attributes = Yii::$app->request->post('MetaTags', Yii::$app->request->get('MetaTags', null));
        if (!$attributes or ! is_array($attributes)) {
            return;
        }
        
        foreach ($attributes as $attribute) {
            $model = $this->getModel($attribute['name']);
            if (!$model->save()) {
                continue;
            }
            if ($this->checkParams($attribute)) {
                foreach ($attribute['params'] as $param => $content) {
                    $metaTagsParam = $this->getParam([
                        'tag_id' => $model->id,
                        'param' => $param,
                    ]);
                    
                    /* Получает настройки текущего мета тега */
                    $options = isset($this->options[$model->name]) ? $this->options[$model->name] : null;
                    
                    if ($content !== null and $content != '') {
                        $metaTagsParam->content = $content;
                    } elseif (isset($this->owner->{$options['default']})) {
                        $this->setDefaultContent($metaTagsParam, $options);
                    } elseif (isset($options['defaultValue'])) {
                        $this->setDefaultValueContent($metaTagsParam, $options);
                    }
                    $metaTagsParam->save();
                }
            } else {
                MetaTagsParams::deleteAll(['tag_id' => $model->id]);
            }
            /* Удаление параметров, которые не были переданы, но есть в базе */
            MetaTagsParams::deleteAll(['AND', 'tag_id = :tag_id', ['NOT IN', 'param', array_keys($attribute['params'])]], [':tag_id' => $model->id]);
        }
        /* Удаление мета тегов, которые не были переданы, но есть в базе */
        MetaTags::deleteAll(['AND', 'model = :model', 'model_id = :model_id', ['NOT IN', 'name', array_keys($attributes)]], [':model' => $this->getModelName(), ':model_id' => $this->owner->id]);
        
    }
    
    /**
     * Устанавливает дефолтное значение мета тегу из определенного аттрибута модели
     * @param MetaTagsParams $metaTagsParam
     * @param array $options
     */
    public function setDefaultContent(&$metaTagsParam, $options) {
        $metaTagsParam->content = $this->owner->{$options['default']};
    }
    
    /**
     * Устанавливает дефолтное значение мета тегу из результата выполнения анонимной функции
     * @param MetaTagsParams $metaTagsParam
     * @param array $options
     */
    public function setDefaultValueContent(&$metaTagsParam, $options) {
        $value = $options['defaultValue']();
        foreach ($this->owner->safeAttributes() as $attr) {
            $value = str_replace('{' . $attr . '}', $this->owner->$attr, $value);
        }
        if (isset($this->owner->id)) {
            $value = str_replace('{id}', $this->owner->id, $value);
        }
        $metaTagsParam->content = $value;
    }

    public function afterDelete($event) {
        MetaTags::deleteAll([
            'model_id' => $this->owner->id,
            'model' => $this->getModelName(),
        ]);
    }

    public function getModel($name) {
        $params = [
            'model_id' => $this->owner->id,
            'name' => $name,
            'model' => $this->getModelName(),
        ];
        $model = MetaTags::find()->with(['params'])->where($params)->one();
        return $model ? $model : new MetaTags($params);
    }

    public function getParam($params) {
        $model = MetaTagsParams::find()->where($params)->one();
        return $model ? $model : new MetaTagsParams($params);
    }

    /**
     * Регистрация title и мета тегов
     * @param boolean $title
     */
    public function register($title = true) {
        foreach ($this->getModels() as $model) {
            if ($model->name == 'title' and $title and isset($model->params[0])) {
                Yii::$app->view->title = $model->params[0]->content;
            }
            $options = [];
            foreach ($model->params as $param) {
                $options = array_merge($options, [$param->param => $param->content]);
            }
            if ($options) {
                $options = array_merge(['name' => $model->name], $options);
                Yii::$app->view->registerMetaTag($options, $model->name);
                
                /** Регистрация og: тегов */
                if (in_array($model->name, ['title', 'description'])) {
                    $options['name'] =  'og:' . $options['name'];
                    Yii::$app->view->registerMetaTag($options, 'og:' . $model->name);
                }
            }
        }
    }
    
    /**
     * Получение мета тегов текущего объекта
     * @return MetaTags
     */
    public function getModels() {
        return MetaTags::find()->with(['params'])->where(['model_id' => $this->owner->id, 'model' => $this->getModelName()])->all();
    }
    
    /**
     * Проверяет присланные параметры мета тега
     * @param array $attribute
     * @return boolean
     */
    private function checkParams($attribute) {
        return isset($attribute['params']) and is_array($attribute['params']) and $attribute['params'];
    }

    /**
     * Получение название текущего объекта
     * @return string
     */
    private function getModelName() {
        return (new \ReflectionClass($this->owner))->getShortName();
    }
    
}
