<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%meta_tags}}`.
 */
class m190208_043558_create_meta_tags_table extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('{{%meta_tags}}', [
            'id' => $this->primaryKey(),
            'model' => $this->string(50)->notNull(),
            'model_id' => $this->integer()->notNull(),
            'name' => $this->string(200)->notNull(),
            'time_update' => $this->integer()->notNull()->defaultValue(0),
        ]);
        
        $this->createTable('{{%meta_tags_params}}', [
            'tag_id' => $this->integer()->notNull(),
            'param' => $this->string(200)->notNull(),
            'content' => $this->text(),
            'status' => $this->boolean()->defaultValue(1),
        ]);
        
        $this->createIndex('idx-meta_tags', '{{%meta_tags}}', ['model', 'model_id', 'name'], true);
        
        $this->createIndex('idx-meta_tags_params', '{{%meta_tags_params}}', 'tag_id');
        $this->addForeignKey('fk-meta_tags_params-tag_id', '{{%meta_tags_params}}', 'tag_id', '{{%meta_tags}}', 'id', 'CASCADE');
        
        $this->addPrimaryKey('pk-meta_tags_params', '{{%meta_tags_params}}', ['tag_id', 'param']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('{{%meta_tags_params}}');
        $this->dropTable('{{%meta_tags}}');
    }

}
