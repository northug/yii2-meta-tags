<?php

namespace northug\metaTags\models\_query;

/**
 * This is the ActiveQuery class for [[\northug\metaTags\models\MetaTags]].
 *
 * @see \northug\metaTags\models\MetaTags
 */
class MetaTagsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \northug\metaTags\models\MetaTags[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \northug\metaTags\models\MetaTags|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
