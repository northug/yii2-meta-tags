<?php

namespace northug\metaTags\models\_query;

use northug\metaTags\models\MetaTagsParams;

/**
 * This is the ActiveQuery class for [[\northug\metaTags\models\MetaTagsParams]].
 *
 * @see \northug\metaTags\models\MetaTagsParams
 */
class MetaTagsParamsQuery extends \yii\db\ActiveQuery {

    public function active() {
        return $this->andWhere([MetaTagsParams::tableName() . '.status' => MetaTagsParams::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     * @return \northug\metaTags\models\MetaTagsParams[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \northug\metaTags\models\MetaTagsParams|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

}
