<?php

namespace northug\metaTags\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use northug\metaTags\models\MetaTagsParams;

/**
 * This is the model class for table "{{%meta_tags}}".
 *
 * @property int $id
 * @property string $model
 * @property int $model_id
 * @property string $name
 * @property int $time_update
 *
 * @property MetaTagsParams[] $params
 */
class MetaTags extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%meta_tags}}';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => false,
                'updatedAtAttribute' => 'time_update',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['model', 'model_id', 'name'], 'required'],
            [['model_id', 'time_update'], 'integer'],
            [['model'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 200],
            [['model', 'model_id', 'name'], 'unique', 'targetAttribute' => ['model', 'model_id', 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'model' => 'Model',
            'model_id' => 'Model ID',
            'name' => 'Name',
            'time_update' => 'Time Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParam($paramName) {
        return $this->hasMany(MetaTagsParams::className(), ['tag_id' => 'id'])->andWhere(['param' => $paramName]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParams() {
        return $this->hasMany(MetaTagsParams::className(), ['tag_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \northug\metaTags\models\_query\MetaTagsQuery the active query used by this AR class.
     */
    public static function find() {
        return new \northug\metaTags\models\_query\MetaTagsQuery(get_called_class());
    }

}
