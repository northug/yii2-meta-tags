<?php

namespace northug\metaTags\models;

use Yii;
use northug\metaTags\models\MetaTags;

/**
 * This is the model class for table "{{%meta_tags_params}}".
 *
 * @property int $tag_id
 * @property string $param
 * @property string $content
 * @property int $status
 *
 * @property MetaTags $tag
 */
class MetaTagsParams extends \yii\db\ActiveRecord {

    const STATUS_ACTIVE = 1, STATUS_INACTIVE = 0;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%meta_tags_params}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['tag_id', 'param'], 'required'],
            [['tag_id', 'status'], 'integer'],
            [['content'], 'string'],
            [['param'], 'string', 'max' => 200],
            [['tag_id', 'param'], 'unique', 'targetAttribute' => ['tag_id', 'param']],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => MetaTags::className(), 'targetAttribute' => ['tag_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'tag_id' => 'Tag ID',
            'param' => 'Param',
            'content' => 'Content',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag() {
        return $this->hasOne(MetaTags::className(), ['id' => 'tag_id']);
    }

    /**
     * {@inheritdoc}
     * @return \northug\metaTags\models\_query\MetaTagsParamsQuery the active query used by this AR class.
     */
    public static function find() {
        return new \northug\metaTags\models\_query\MetaTagsParamsQuery(get_called_class());
    }

}
