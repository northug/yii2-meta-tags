<?php

namespace northug\metaTags\assets;
use yii\web\AssetBundle;

class MetaAsset extends AssetBundle
{
    public $sourcePath = '@northug/metaTags/web';
    public $css = [
        'css/meta-tags.css',
    ];
    public $js = [
        //'js/meta-tags.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}