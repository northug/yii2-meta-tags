<?php

/* @var $this yii\web\View */
/* @var $model object */
/* @var $attribute string */
/* @var $attributes array */
/* @var $optionsInput array */

use yii\helpers\Html;
use northug\metaTags\assets\MetaAsset;

MetaAsset::register($this);

$formName = $model->formName();

echo Html::beginTag('div', ['class' => 'meta-tag-block']);

foreach ($attributes as $item) {
    echo $this->render('item', ['param' => $item, 'model' => $model]);
}

echo Html::endTag('div');
