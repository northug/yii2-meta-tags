<?php

/* @var $this yii\web\View */
/* @var $param array */
/* @var $model object */

use yii\helpers\Html;

$metaTag = $model->getBehavior('MetaTag')->getModel($param['name']);

$paramName = 'MetaTags[' . $param['name'] . ']';

echo Html::beginTag('div', ['class' => 'meta-tag-item']);
echo Html::hiddenInput($paramName . '[name]', $param['name']);
echo Html::tag('label', ucfirst($param['name']));
if ($param['type'] == 'text') {
    $value = $metaTag->getParam('content')->active()->one()->content;
    echo Html::input('text', $paramName . '[params][content]', $value, $param['options']);
} elseif ($param['type'] == 'textarea') {
    $value = $metaTag->getParam('content')->active()->one()->content;
    echo Html::textarea($paramName . '[params][content]', $value, $param['options']);
}
echo Html::endTag('div');
