<?php

namespace northug\metaTags\widget;

use northug\metaTags\models\MetaTags;

/**
 * Description of MetaTagsWidget
 *
 * @author Пользователь
 */
class MetaTagsWidget extends \yii\widgets\InputWidget {

    public $model;
    public $optionsInput = ['class' => 'form-control'];
    public $attributes = [
        'title' => [
            'name' => 'title',
            'type' => 'text',
            'options' => ['class' => 'form-control'],
        ],
        'description' => [
            'name' => 'description',
            'type' => 'textarea',
            'options' => ['class' => 'form-control', 'rows' => 4],
        ],
        'keywords' => [
            'name' => 'keywords',
            'type' => 'textarea',
            'options' => ['class' => 'form-control', 'rows' => 4],
        ],
    ];
    public $extraAttributes = [];

    public function init() {
        $this->attributes = array_merge($this->attributes, $this->extraAttributes);
    }

    public function run() {
        return $this->render('index', [
                    'model' => $this->model,
                    'attributes' => $this->attributes,
                    'optionsInput' => $this->optionsInput,
        ]);
    }

}
