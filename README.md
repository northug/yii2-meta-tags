Meta Tags and Title
===================
Add Meta Tags and Title in model

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist northug/yii2-meta-tags "*"
```

or add

```
"northug/yii2-meta-tags": "*"
```

to the require section of your `composer.json` file.

Migrate:
```php
yii migrate --migrationPath=@northug/metaTags/migrations
```

Usage
-----

Once the extension is installed, simply use it in your code by  :

Add `MetaTagBehavior` to your model, and configure it.

```php
public function behaviors()
{
    return [
        'MetaTag' => [
            'class' => MetaTagBehavior::className(),
        ],
    ];
}
```

In form

```
<?= $form->field($model, 'meta')->widget(MetaTagsWidget::class)->label('SEO') ?>
```

and registar tags

```php
$model = Model::findOne(1);

$model->getBehavior('MetaTag')->register();
```